--Mods include: Large Pinecone
require "prefabutil"
local assets =
{
	Asset("ANIM", "anim/pinecone.zip"),
}

local function growtree(inst)
	print ("GROWTREE")
    inst.growtask = nil
    inst.growtime = nil
	local tree = SpawnPrefab("largeevergreen_tall") 
    if tree then 
		tree.Transform:SetPosition(inst.Transform:GetWorldPosition() ) 
        tree:growfromseed()--PushEvent("growfromseed")
        inst:Remove()
	end
end

local function plant(inst, growtime)
    inst:RemoveComponent("inventoryitem")
    inst.AnimState:PlayAnimation("idle_planted")
    inst.SoundEmitter:PlaySound("dontstarve/wilson/plant_tree")
    inst.growtime = GetTime() + growtime
    print ("PLANT", growtime)
    inst.growtask = inst:DoTaskInTime(growtime, growtree)
end

local function oldondeploy(inst, pt) 
    --inst = inst.components.stackable:Get()
	inst  = SpawnPrefab("largepinecone")
    inst.Transform:SetPosition(pt:Get() )
    --local timeToGrow = GetRandomWithVariance(TUNING.PINECONE_GROWTIME.base, TUNING.PINECONE_GROWTIME.random)
	local timeToGrow = 5
    plant(inst, timeToGrow)
	
	--tell any nearby leifs to chill out
	local ents = TheSim:FindEntities(pt.x,pt.y,pt.z, TUNING.LEIF_PINECONE_CHILL_RADIUS, {"leif"})
	
	local played_sound = false
	for k,v in pairs(ents) do
		
		local chill_chance = TUNING.LEIF_PINECONE_CHILL_CHANCE_FAR
		if distsq(pt, Vector3(v.Transform:GetWorldPosition())) < TUNING.LEIF_PINECONE_CHILL_CLOSE_RADIUS*TUNING.LEIF_PINECONE_CHILL_CLOSE_RADIUS then
			chill_chance = TUNING.LEIF_PINECONE_CHILL_CHANCE_CLOSE
		end
	
		if math.random() < chill_chance then
			if v.components.sleeper then
				v.components.sleeper:GoToSleep(1000)
			end
		else
			if not played_sound then
				v.SoundEmitter:PlaySound("dontstarve/creatures/leif/taunt_VO")
				played_sound = true
			end
		end
		
	end
	
end

local function PlantIt(inst, v)
    local test = inst.components.deployable:CanDeploy(Point(v.x, v.y, v.z))
    if test then
        inst.components.deployable:OldDeploy(Point(v.x, v.y, v.z), GetPlayer())
        for k,val in pairs(TheSim:FindEntities(v.x, v.y, v.z, 0.5)) do
          if val.components.pickable or val.components.harvestable or val.components.hackable then
            return val
          end
        end
        return true
    else
      return false
    end
end

local function getPoints(offset, center, num)
  --Get the center reference point
  local refpt = center
  local root  = math.floor(math.sqrt(num))
  local size  = {x = root, z = root}
  --Grid size from options, use as boundaries for grid
  local xop, zop = size.x, size.z
  --Set start position for grid ('bottem-left')
  local xshift, zshift = (xop/2 - 0.5), (zop/2 - 0.5)
  local xstart, zstart = (refpt.x - xshift - (xshift * offset)), (refpt.z - zshift - (zshift * offset))
  --Set the spawn points
  local spawn_pts= {}
  for kz = 0, zop - 1, 1 do
      local osx, osz = offset, offset
      for kx = 0, xop - 1, 1 do
          table.insert(spawn_pts, { x = (xstart + (kx * osx) + kx), y = (refpt.y), z = (zstart + (kz * osz) + kz) })
      end
  end

  return spawn_pts
end

local function onfinished(inst)
    inst:Remove()
end

  --local old_ondeploy = inst.components.deployable.ondeploy
  local function ondeploy(inst, pt)
    local player = GetPlayer()
    --local item   = player.components.inventory:GetActiveItem()
    local num    = 36
	local flag = false
	local old_ondeploy = inst.components.deployable.oldondeploy
	local offset = 1

      flag              = true
      num               = num +1
      local points      = getPoints(offset, pt, num)
      local cone        = nil
      local pt          = nil
      local plant       = nil
      local toFertilize = {}

      while num > 0 and table.getn(points) > 0 do
        cone  = SpawnPrefab("largepinecone")
        pt    = table.remove(points, 1)
        plant = PlantIt(cone, pt)
        if plant then
          table.insert(toFertilize, plant)
          num = num-1
        end

        --inst.components.stackable:SetStackSize(num)
      end

      --DeductPlantableFromActiveItem(num)
	  onfinished(inst)
      flag = false
  end

local function stopgrowing(inst)
    if inst.growtask then
        inst.growtask:Cancel()
        inst.growtask = nil
    end
    inst.growtime = nil
end


local notags = {'NOBLOCK', 'player', 'FX'}
local function test_ground(inst, pt)
	local tiletype = GetGroundTypeAtPosition(pt)
	local ground_OK = tiletype ~= GROUND.ROCKY and tiletype ~= GROUND.ROAD and tiletype ~= GROUND.IMPASSABLE and
						tiletype ~= GROUND.UNDERROCK and tiletype ~= GROUND.WOODFLOOR and 
						tiletype ~= GROUND.CARPET and tiletype ~= GROUND.CHECKER and tiletype < GROUND.UNDERGROUND
	
	if ground_OK then
	    local ents = TheSim:FindEntities(pt.x,pt.y,pt.z, 4, nil, notags) -- or we could include a flag to the search?
		local min_spacing = inst.components.deployable.min_spacing or 2

	    for k, v in pairs(ents) do
			if v ~= inst and v:IsValid() and v.entity:IsVisible() and not v.components.placer and v.parent == nil then
				if distsq( Vector3(v.Transform:GetWorldPosition()), pt) < min_spacing*min_spacing then
					return false
				end
			end
		end
		return true
	end
	return false
end

local function describe(inst)
    if inst.growtime then
        return "PLANTED"
    end
end

local function displaynamefn(inst)
    if inst.growtime then
        return STRINGS.NAMES.PINECONE_SAPLING
    end
    return STRINGS.NAMES.LARGEPINECONE
end

local function OnSave(inst, data)
    if inst.growtime then
        data.growtime = inst.growtime - GetTime()
    end
end

local function OnLoad(inst, data)
    if data and data.growtime then
        plant(inst, data.growtime)
    end
end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("pinecone")
    inst.AnimState:SetBuild("pinecone")
    inst.AnimState:PlayAnimation("idle")
    

    inst:AddComponent("edible")
    inst.components.edible.foodtype = "WOOD"
    inst.components.edible.woodiness = 2

    --inst:AddComponent("stackable")
	--inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("inspectable")
    inst.components.inspectable.getstatus = describe
    
    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = TUNING.LARGE_FUEL
    
	MakeSmallBurnable(inst, TUNING.LARGE_BURNTIME)
	inst:ListenForEvent("onignite", stopgrowing)
    MakeSmallPropagator(inst)
    
    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "pinecone"
    
    inst:AddComponent("deployable")
    inst.components.deployable.test = test_ground
    inst.components.deployable.ondeploy = ondeploy
	inst.components.deployable.oldondeploy = oldondeploy
    
    inst.displaynamefn = displaynamefn

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab( "common/inventory/largepinecone", fn, assets),
	   MakePlacer( "common/largepinecone_placer", "pinecone", "pinecone", "idle_planted" ) 