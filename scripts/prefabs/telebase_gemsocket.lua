--Mods include: SuperTelestaff
assets = 
{
    Asset("ANIM", "anim/staff_purple_base.zip"),
}

local function DestroyGem(inst) --start charging
    inst:DoTaskInTime(math.random() * 0.5, function() 
        inst.AnimState:PlayAnimation("shatter")
        inst.AnimState:PushAnimation("idle_empty")
        inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
		inst.components.activated.charged = false
    end)
end

local function RestoreGem(inst) --finished charging
	if inst.components.activated.activated == true then
		inst:DoTaskInTime(math.random() * 0.5, function() 
			inst.AnimState:PlayAnimation("shatter")
			inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
			inst.AnimState:PlayAnimation("idle_full_loop", true)
			inst.SoundEmitter:PlaySound("dontstarve/common/telebase_gemplace")
			inst.SoundEmitter:PlaySound("dontstarve/common/telebase_hum", "hover_loop")
			inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
		end)
	else
		inst.AnimState:PlayAnimation("shatter")
		inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
		inst.SoundEmitter:KillSound("hover_loop")
		inst.AnimState:PlayAnimation("idle_empty")
		inst.AnimState:ClearBloomEffectHandle()
	end
	inst.components.activated.charged = true
	
end

local function OnGemGivenLoad(inst)
	if inst.components.activated.charged == false then
        inst.AnimState:PlayAnimation("shatter")
        inst.AnimState:PushAnimation("idle_empty")
        inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
	else
	inst.AnimState:PlayAnimation("idle_full_loop", true)
    inst.SoundEmitter:PlaySound("dontstarve/common/telebase_gemplace")
	end
	inst.SoundEmitter:PlaySound("dontstarve/common/telebase_hum", "hover_loop")
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
end

local function OnGemTakenLoad(inst)
	inst.SoundEmitter:KillSound("hover_loop")
	inst.AnimState:PlayAnimation("idle_empty")
	inst.AnimState:ClearBloomEffectHandle()
end

local function OnLoad(inst, data)
    if inst.components.activated.activated == true then
        OnGemGivenLoad(inst)  
    else
        OnGemTakenLoad(inst)
    end
end

local function getstatus(inst) --used only for the examine handle... and now the on load method.
    --if inst.components.pickable.caninteractwith then
	if inst.components.activated.activated == true and inst.components.activated.charged == false then
		return "CHARGING"
	elseif inst.components.activated.activated == true then
        return "VALID"
	else
        return "EMPTY"
    end
end

local function fn()
    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
    local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    anim:SetBank("staff_purple_base")
    anim:SetBuild("staff_purple_base")
    anim:PlayAnimation("idle_empty")
    
    inst:AddComponent("inspectable")
    inst.components.inspectable.getstatus = getstatus

    --[[inst:AddComponent("pickable")
    inst.components.pickable.caninteractwith = false
    inst.components.pickable.onpickedfn = OnGemTaken

    inst:AddComponent("trader")
    inst.components.trader:SetAcceptTest(ItemTradeTest)
    inst.components.trader.onaccept = OnGemGiven]]
	
    inst:AddComponent("activated")

    inst.DestroyGemFn = DestroyGem
    inst.RestoreGemFn = RestoreGem
    --inst.OnGemTakenLoadFn = OnGemTakenLoad
    --inst.OnGemGivenLoadFn = OnGemGivenLoad

    inst.OnLoad = OnLoad

    return inst
end  

return Prefab( "forest/objects/telebase/gemsocket", fn, assets)


