--Mods include: Equpiable compass
local assets=
{
	Asset("ANIM", "anim/compass.zip"),
	Asset("ANIM", "anim/swap_compass.zip"),
}

local function GetDirection()
    
    
    local heading = TheCamera:GetHeading()--inst.Transform:GetRotation() 
    local dirs = 
    {
        N=0, S=180,
        NE=45, E=90, SE=135,
        NW=-45, W=-90, SW=-135, 
    }
    local dir, closest_diff = nil, nil

    for k,v in pairs(dirs) do
        local diff = math.abs(anglediff(heading, v))
        if not dir or diff < closest_diff then
            dir, closest_diff = k, diff
        end
    end
    return dir
end


local function GetStatus(inst, viewer)
    return GetDirection()
end

local function onequip(inst, owner)
	inst:DoTaskInTime(0, function() -- TheSim can't be called before load so this makes sure the game is loaded before trying.
		if TheSim then --Probably unnecessary, but this is just extra safety to prevent really rare and unusual crashes.
			local minimap = TheSim:FindFirstEntityWithTag("minimap")
			
			if minimap then
				minimap.MiniMap:EnableFogOfWar(false)
			end
		end
	end)
    owner.AnimState:OverrideSymbol("swap_object", "swap_compass", "swap_lighter")
    owner.AnimState:Show("ARM_carry") 
    owner.AnimState:Hide("ARM_normal") 
end

local function onunequip(inst, owner) 
	inst:DoTaskInTime(0, function() --not as important as the onequip, but better safe then sorry amirite?
		if TheSim then
			local minimap = TheSim:FindFirstEntityWithTag("minimap")
			local mapportato = TheSim:FindFirstEntityWithTag("mapportato") --this adds compatibility for my mapportato mod for people who like both, personaly I am a Better Compass kind of guy.
			
			if minimap and not mapportato then
				minimap.MiniMap:EnableFogOfWar(true)
			end
		end
    end)
	owner.AnimState:Hide("ARM_carry") 
    owner.AnimState:Show("ARM_normal") 
end

local function fn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    anim:SetBank("compass")
    anim:SetBuild("compass")
    anim:PlayAnimation("idle")
    
    MakeInventoryPhysics(inst)
    
    inst:AddComponent("inventoryitem")
    inst:AddComponent("inspectable")
    --inst.components.inspectable.noanim = true
    inst.components.inspectable.getstatus = GetStatus
	
	inst:AddComponent("equippable")
	inst.components.equippable:SetOnEquip( onequip )
    inst.components.equippable:SetOnUnequip( onunequip )
    
    return inst
end

return Prefab( "common/inventory/compass", fn, assets)
