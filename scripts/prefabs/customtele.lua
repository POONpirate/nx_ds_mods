--Mods include: SuperTelestaff
local assets=
{
	Asset("ANIM", "anim/customtele.zip"),
}

local function buildgem(number)
    local fn = function()
        --[[local Sparkle = function(inst)
            inst.AnimState:PlayAnimation(number.."customtele_sparkle")
            inst.AnimState:PushAnimation(number.."customtele_idle")
            inst.sparkletask = inst:DoTaskInTime(4 + math.random(), inst.sparklefn)
        end

        local StartSparkling = function(inst)
            inst.sparkletask = inst:DoTaskInTime(1, inst.sparklefn)
        end

        local StopSparkling = function(inst)
            if inst.sparkletask then
                inst.sparkletask:Cancel()
                inst.sparkletask = nil
            end
        end]]
    	local inst = CreateEntity()
    	inst.entity:AddTransform()
    	inst.entity:AddAnimState()
		local trans = inst.entity:AddTransform()
		local anim = inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
		
		inst.AnimState:SetBank("blue")
        inst.AnimState:SetBuild("customtele")
        inst.AnimState:PlayAnimation(number.."customtele_idle")
        MakeInventoryPhysics(inst)

        --inst:AddComponent("edible")
        --inst.components.edible.foodtype = "ELEMENTAL"
        inst:AddComponent("tradable")
        --inst.components.edible.hungervalue = 15
        
	    --inst:AddComponent("repairer")
	    --inst.components.repairer.repairmaterial = "gem"
	    --inst.components.repairer.workrepairvalue = TUNING.REPAIR_GEMS_WORK

        --inst:AddComponent("stackable")
    	--inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

        inst:AddComponent("inspectable")
        
        inst:AddComponent("inventoryitem")
		
        --inst.sparklefn = Sparkle
        
        --StartSparkling(inst)
        
        return inst
    end

    return fn
end

return
 Prefab( "common/inventory/tele1", buildgem("1"), assets),
 Prefab( "common/inventory/tele2", buildgem("2"), assets),
 Prefab( "common/inventory/tele3", buildgem("3"), assets),
 Prefab( "common/inventory/tele4", buildgem("4"), assets),
 Prefab( "common/inventory/tele5", buildgem("5"), assets)
