--Mods include: SuperTelestaff
--Component for the SuperTelebase (Telebase+)
local Cteletarget = Class(function(self, inst)
    self.inst = inst
    self.cteletarget1 = false
    self.cteletarget2 = false
    self.cteletarget3 = false
    self.cteletarget4 = false
    self.cteletarget5 = false
end)


function Cteletarget:SetBool( boolean, teleporterslot )
	if teleporterslot == 1 then
		self.cteletarget1 = boolean
	end
	if teleporterslot == 2 then
		self.cteletarget2 = boolean
	end
	if teleporterslot == 3 then
		self.cteletarget3 = boolean
	end
	if teleporterslot == 4 then
		self.cteletarget4 = boolean
	end
	if teleporterslot == 5 then
		self.cteletarget5 = boolean
	end
end

function Cteletarget:OnSave()
	local data = { }
		data.cteletarget1 = self.cteletarget1
		data.cteletarget2 = self.cteletarget2
		data.cteletarget3 = self.cteletarget3
		data.cteletarget4 = self.cteletarget4
		data.cteletarget5 = self.cteletarget5
	return data
end

function Cteletarget:OnLoad( data )
	if data then
		self:SetBool( data.cteletarget1, 1 )
		self:SetBool( data.cteletarget2, 2 )
		self:SetBool( data.cteletarget3, 3 )
		self:SetBool( data.cteletarget4, 4 )
		self:SetBool( data.cteletarget5, 5 )
	end
end

return Cteletarget
