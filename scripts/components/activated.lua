--Mods include: SuperTelestaff
local Activated = Class(function(self, inst)
    self.inst = inst
    self.activated = false
	self.charged = true
end)

function Activated:SetBool( boolean )
		self.activated = boolean
end

function Activated:OnSave()
	local data = { }
		data.activated = self.activated
		data.charged = self.charged
	return data
end

function Activated:OnLoad( data )
	if data then
		self.activated = data.activated
		self.charged = data.charged
	end
end

return Activated
