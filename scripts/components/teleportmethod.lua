--Mods include: SuperTelestaff
local Teleportmethod = Class(function(self, inst) --used only for super telestaff
    self.inst = inst
    self.method = 0
	self.finite = nil
	self.teleswitch = nil
end)

function Teleportmethod:SetTeleportmethod( m )
		self.method = m
end

function Teleportmethod:SetFinite( b )
		self.finite = b
end

function Teleportmethod:SetFn(fn)
	self.teleswitch = fn
end

function Teleportmethod:UseFn(target, pos)
	if self.teleswitch then
		self.teleswitch(self.inst, target, pos)
	end
end

function Teleportmethod:OnSave()
	local data = { }
		data.finite = self.finite
	return data
end

function Teleportmethod:OnLoad( data )
	if data then
		--self:SetTeleportmethod( data.method )
		self:SetTeleportmethod( 0 )
		self:SetFinite ( data.finite )
	end
end

return Teleportmethod
