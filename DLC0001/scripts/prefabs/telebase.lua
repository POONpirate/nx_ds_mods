--Mods include: SuperTelestaff
local assets =
{
	Asset("ANIM", "anim/staff_purple_base_ground.zip"),
}

local prefabs =
{
	"gemsocket",
	"collapse_small",
}

local function startcharging(inst) --destroys gems then sets the minimap icon
	for k,v in pairs(inst.components.objectspawner.objects) do
		if v.DestroyGemFn then
			v.DestroyGemFn(v)
		end
	end
end

local function oncharged(inst) --sets minimap icon
	for k,v in pairs(inst.components.objectspawner.objects) do
		if v.RestoreGemFn then
			v.RestoreGemFn(v)
		end
	end
end

local function validcustomteleporttarget(inst)
		if inst.components.pickable and inst.components.pickable.caninteractwith and inst.components.cooldown.charged then
			return true
		elseif inst.components.pickable and inst.components.pickable.caninteractwith and not inst.components.cooldown.charged then
			return "CHARGING"
		else
			return false
		end
end

local function validcteletarget(inst, target)
	if target == 1 and inst.components.cteletarget.cteletarget1 then return true
	elseif target == 2 and inst.components.cteletarget.cteletarget2 then return true
	elseif target == 3 and inst.components.cteletarget.cteletarget3 then return true
	elseif target == 4 and inst.components.cteletarget.cteletarget4 then return true
	elseif target == 5 and inst.components.cteletarget.cteletarget5 then return true
	else return false
	end
end

local function getstatus(inst)
	--if validcustomteleporttarget(inst) then
		--return "VALID"
	if validcustomteleporttarget(inst) == "CHARGING" then
		return "CHARGING"
	elseif inst.components.cteletarget.cteletarget1 then
		return "TELE1"
	elseif inst.components.cteletarget.cteletarget2 then
		return "TELE2"
	elseif inst.components.cteletarget.cteletarget3 then
		return "TELE3"
	elseif inst.components.cteletarget.cteletarget4 then
		return "TELE4"
	elseif inst.components.cteletarget.cteletarget5 then
		return "TELE5"
	else
		return "EMPTY"
	end
end

local telebase_parts = {
	{part = "gemsocket", x= -1.6, z=-1.6},
	{part = "gemsocket", x=2.7, z=-0.8},
	{part = "gemsocket", x=-0.8, z= 2.7},
}

local function removesockets(inst)
	for k,v in pairs(inst.components.objectspawner.objects) do
		v:Remove()
	end
end

local function ondestroyed(inst)
	--[[for k,v in pairs(inst.components.objectspawner.objects) do
		if v.components.pickable and v.components.pickable.caninteractwith then
			inst.components.lootdropper:AddChanceLoot("purplegem", 1)	
		end
	end]]
	inst.components.lootdropper:DropLoot()
	SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
	inst:Remove()
end

local function onhit(inst)
	for k,v in pairs(inst.components.objectspawner.objects) do
		if v.components.pickable and v.components.pickable.caninteractwith then
			v.AnimState:PlayAnimation("hit_full")
			v.AnimState:PushAnimation("idle_full_loop")
		else
			v.AnimState:PlayAnimation("hit_empty")
			v.AnimState:PushAnimation("idle_empty")
		end
	end
end

local function OnGemChange(inst) --bloom around the gem sockets when numbered AND charged
	if validcustomteleporttarget(inst) == true --[[or validcustomteleporttarget(inst) == "CHARGING"]] then
		for k,v in pairs(inst.components.objectspawner.objects) do
    		v.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
		end
	else
		for k,v in pairs(inst.components.objectspawner.objects) do
    		v.AnimState:ClearBloomEffectHandle()
		end
	end
end

local function OnKeyChange(inst) --bloom around the center of the telebase when numbered or charging
	if validcustomteleporttarget(inst) == true or validcustomteleporttarget(inst) == "CHARGING" then
    	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
	else
    		inst.AnimState:ClearBloomEffectHandle()
	end
end

---------------------------------------------------------

local function ResetCustomTeleTarget(inst)
	if inst.components.cteletarget.cteletarget1 then
		inst.components.cteletarget.cteletarget1 = false
		--if not validcteletarget1(inst) then print "This TeleBase is no longer marked as custom teleport 1." end
    end
	if inst.components.cteletarget.cteletarget2 then
		inst.components.cteletarget.cteletarget2 = false
    end
	if inst.components.cteletarget.cteletarget3 then
		inst.components.cteletarget.cteletarget3 = false
    end
	if inst.components.cteletarget.cteletarget4 then
		inst.components.cteletarget.cteletarget4 = false
    end
	if inst.components.cteletarget.cteletarget5 then
		inst.components.cteletarget.cteletarget5 = false
    end
end

local function ItemTradeTest(inst, item)
    if  item.prefab == "cutgrass" or item.prefab == "twigs" or item.prefab == "flint" or item.prefab == "log" or item.prefab == "rocks" then
        return true
    end
    return false
end

local function SetupItem(inst, item)
    if item == 1 then
        return "cutgrass"
    elseif item == 2 then
        return "twigs"
    elseif item == 3 then
        return "flint"
    elseif item == 4 then
        return "log"
    elseif item == 5 then
        return "rocks"
	else
		return false
	end
end

local function OnGemGiven(inst, giver, item)
	local player = GetPlayer()
	local pt = player:GetPosition()
    local superents = TheSim:FindEntities(pt.x,pt.y,pt.z, 9000, {"telebase"})
	for k,v in pairs(superents) do
		if item.prefab == SetupItem(inst, 1) and v.components.cteletarget.cteletarget1 then
			player.components.talker:Say(GetString(player.prefab, "FULLCHANNEL"))
			inst.components.lootdropper:SpawnLootPrefab(item.prefab)
			return
		elseif item.prefab == SetupItem(inst, 2) and v.components.cteletarget.cteletarget2 then
			player.components.talker:Say(GetString(player.prefab, "FULLCHANNEL"))
			inst.components.lootdropper:SpawnLootPrefab(item.prefab)
			return
		elseif item.prefab == SetupItem(inst, 3) and v.components.cteletarget.cteletarget3 then
			player.components.talker:Say(GetString(player.prefab, "FULLCHANNEL"))
			inst.components.lootdropper:SpawnLootPrefab(item.prefab)
			return
		elseif item.prefab == SetupItem(inst, 4) and v.components.cteletarget.cteletarget4 then
			player.components.talker:Say(GetString(player.prefab, "FULLCHANNEL"))
			inst.components.lootdropper:SpawnLootPrefab(item.prefab)
			return
		elseif item.prefab == SetupItem(inst, 5) and v.components.cteletarget.cteletarget5 then
			player.components.talker:Say(GetString(player.prefab, "FULLCHANNEL"))
			inst.components.lootdropper:SpawnLootPrefab(item.prefab)
			return
		end
	end

	for k,v in pairs(inst.components.objectspawner.objects) do
		v.SoundEmitter:PlaySound("dontstarve/common/telebase_hum", "hover_loop")
		v.AnimState:PlayAnimation("idle_full_loop", true)
		if v.components.activated.activated == false then
			v.components.activated:SetBool( true ) 
		end
	end
    inst.SoundEmitter:PlaySound("dontstarve/common/telebase_gemplace")
    inst.components.trader:Disable()
    --inst.components.pickable:SetUp("purplegem", 1000000)

	ResetCustomTeleTarget(inst)
	if item.prefab == SetupItem(inst, 1) then
		--inst.components.cteletarget:SetBool(true, 1)
		inst.components.cteletarget.cteletarget1 = true
		local GemGiveItem = SetupItem(inst, 1)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	elseif item.prefab == SetupItem(inst, 2) then
		inst.components.cteletarget.cteletarget2 = true
		local GemGiveItem = SetupItem(inst, 2)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	elseif item.prefab == SetupItem(inst, 3) then
		inst.components.cteletarget.cteletarget3 = true
		local GemGiveItem = SetupItem(inst, 3)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	elseif item.prefab == SetupItem(inst, 4) then
		inst.components.cteletarget.cteletarget4 = true
		local GemGiveItem = SetupItem(inst, 4)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	elseif item.prefab == SetupItem(inst, 5) then
		inst.components.cteletarget.cteletarget5 = true
		local GemGiveItem = SetupItem(inst, 5)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	end
	
    inst.components.pickable:Pause()
    inst.components.pickable.caninteractwith = true
	
	if not inst.components.cooldown.charged then
		startcharging(inst)
	end
	
end

local function OnGemTaken(inst)
	for k,v in pairs(inst.components.objectspawner.objects) do
		v.SoundEmitter:KillSound("hover_loop")
		v.AnimState:PlayAnimation("idle_empty")
		if v.components.activated.activated == true then
			v.components.activated:SetBool( false ) 
		end
	end
    inst.components.trader:Enable()
    inst.components.pickable.caninteractwith = false
	ResetCustomTeleTarget(inst)
end

local function OnGemGivenLoad(inst)
	--print "GemGiven Called on load for Super TeleBase."
	--[[for k,v in pairs(inst.components.objectspawner.objects) do
		print "this print check :p"
		if v.OnGemGivenLoadFn then
			v.OnGemGivenLoadFn(v)
		end
	end]]
	
    inst.components.trader:Disable()
    inst.components.pickable:Pause()
    inst.components.pickable.caninteractwith = true
	
	if inst.components.cteletarget.cteletarget1 then
		local GemGiveItem = SetupItem(inst, 1)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	elseif inst.components.cteletarget.cteletarget2 then
		local GemGiveItem = SetupItem(inst, 2)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	elseif inst.components.cteletarget.cteletarget3 then
		local GemGiveItem = SetupItem(inst, 3)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	elseif inst.components.cteletarget.cteletarget4 then
		local GemGiveItem = SetupItem(inst, 4)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	elseif inst.components.cteletarget.cteletarget5 then
		local GemGiveItem = SetupItem(inst, 5)
		inst.components.pickable:SetUp(GemGiveItem, 1000000)
	end
	if not inst.components.cooldown.charged then
		startcharging(inst)
	end
end

local function OnGemTakenLoad(inst)
	--print "GemTaken Called on load for Super TeleBase."
	for k,v in pairs(inst.components.objectspawner.objects) do
		v.SoundEmitter:KillSound("hover_loop")
		v.AnimState:PlayAnimation("idle_empty")
		if v.components.activated.activated == true then
			v.components.activated:SetBool( false ) 
		end
	end
	
    inst.components.trader:Enable()
    inst.components.pickable.caninteractwith = false
	ResetCustomTeleTarget(inst)
end

local function OnLoad(inst, data)
    if not inst.components.pickable.caninteractwith then
        OnGemTakenLoad(inst)  
    else
        OnGemGivenLoad(inst)
    end
	OnGemChange(inst)
	OnKeyChange(inst)
end
---------------------------------------------------------

--[[local function NewObject(inst, obj)
	inst:ListenForEvent("trade", function() OnGemChange(inst) end, obj)
	inst:ListenForEvent("picked", function() OnGemChange(inst) end, obj)

	OnGemChange(inst)
end]]

local function commonfn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()

	local minimap = inst.entity:AddMiniMapEntity()
	minimap:SetIcon("telebase.png")

    MakeInventoryPhysics(inst)

    inst:AddTag("telebase")

    anim:SetBuild("staff_purple_base_ground")
    anim:SetBank("staff_purple_base_ground")
    anim:PlayAnimation("idle")
	anim:SetOrientation( ANIM_ORIENTATION.OnGround )
	anim:SetLayer( LAYER_BACKGROUND )
	anim:SetSortOrder( 3 )
	trans:SetRotation( 45 )

	--inst.canteleto = validteleporttarget
	inst.canteleto = validcustomteleporttarget
	inst.cancustomteleto = validcustomteleporttarget
	inst.customteletarget = validcteletarget

	inst:AddComponent("inspectable")
	inst.components.inspectable.getstatus = getstatus
	
	--------------------------------------------------- Custom Slots
    inst:AddComponent("pickable")
    inst.components.pickable.caninteractwith = false
    inst.components.pickable.onpickedfn = OnGemTaken
	inst:ListenForEvent("picked", OnGemChange)
	inst:ListenForEvent("picked", OnKeyChange)

    inst:AddComponent("trader")
    inst.components.trader:SetAcceptTest(ItemTradeTest)
    inst.components.trader.onaccept = OnGemGiven
	inst:ListenForEvent("trade", OnGemChange)
	inst:ListenForEvent("trade", OnKeyChange)
	
    inst:AddComponent("cooldown")
		inst.components.cooldown.cooldown_duration = TUNING.SUPERTELEBASECOOLDOWN
		inst.components.cooldown.charged = true
		inst.components.cooldown.onchargedfn = oncharged
		inst.components.cooldown.startchargingfn = startcharging
	
    inst:AddComponent("cteletarget")
	---------------------------------------------------

	inst:AddComponent("workable")
	inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
	inst.components.workable:SetWorkLeft(4)
	inst.components.workable:SetOnWorkCallback(onhit)
	inst.components.workable:SetOnFinishCallback(ondestroyed)

	inst:AddComponent("lootdropper")
    
    inst:AddComponent("objectspawner")
    --inst.components.objectspawner.onnewobjectfn = NewObject

    inst:ListenForEvent("onbuilt", function()

	    local pos = inst:GetPosition()         
	    for k,v in pairs(telebase_parts) do
			local part = inst.components.objectspawner:SpawnObject(v.part)
			part.Transform:SetPosition(pos.x + v.x, 0, pos.z + v.z)
	    end 

    	for k,v in pairs(inst.components.objectspawner.objects) do
    		v:Hide()
    		v:DoTaskInTime(math.random() * 0.5, function() v:Show() v.AnimState:PlayAnimation("place")
    		v.AnimState:PushAnimation("idle_empty") end)
    	end
	end)

	inst:ListenForEvent("onremove", removesockets)
	
    inst.OnLoad = OnLoad

	return inst
end

return Prefab( "common/inventory/telebase", commonfn, assets, prefabs),
	   MakePlacer( "common/telebase_placer", "staff_purple_base_ground", "staff_purple_base_ground", "idle" ) 